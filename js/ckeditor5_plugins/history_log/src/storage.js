// Storage utilities to save and load data from localStorage.

import { generateHash } from './utility'

/**
 * Create the storage key that will be used to save the data.
 *
 * @param {module:core/editor/editor~Editor} editor
 *
 * @returns {string}
 */
export function createStorageKey (editor) {
  const config = editor.config.get('historyLog')

  const prefix = config.saveKeyPrefix
  const ignoreParameters = config.saveKeyIgnoreParams
  const url = ignoreParameters ? removeUrlParameters(window.location.href) : window.location.href
  const delimiter = config.saveKeyDelimiter
  const attributeSelector = config.saveKeyAttribute

  const storageKey = prefix + delimiter + url + delimiter + editor.sourceElement.getAttribute(attributeSelector)
  editor.config.set('historyLog.saveKey', storageKey)
}

/**
 * Get the storage key that will be used to save the data.
 *
 * @param {module:core/editor/editor~Editor} editor
 *
 * @returns {string}
 */
export function getStorageKey (editor) {
  return editor.config.get('historyLog.saveKey')
}

/**
 * Load data from localStorage.
 *
 * @param {string} storageKey
 *
 * @returns {Promise<array|null>}
 */
export async function loadData (storageKey) {
  const compressedJSON = localStorage.getItem(storageKey)

  if (!compressedJSON) {
    return null
  }

  const data = await decompress(compressedJSON)
  return data.logs ?? []
}

/**
 * Save data to localStorage.
 *
 * @param {string} storageKey
 * @param {module:core/editor/editor~Editor} editor
 * @param {string} initiatorType
 *
 * @returns {Promise<void>}
 */
export async function saveData (storageKey, editor, initiatorType = 'autosave') {
  const data = editor.getData()

  const newData = {
    id: Date.now(),
    saveTime: new Date(),
    type: initiatorType,
    dataHash: generateHash(data),
    data,
  }

  let dataHistory = await loadData(storageKey) ?? []

  // Don't autosave immediately after a rollback.
  if (initiatorType === 'autosave' && dataHistory.length > 0) {
    const lastData = dataHistory[dataHistory.length - 1]
    const delayTime = editor.config.get('historyLog.waitingTime') + 2000
    const timeSinceRollback = newData.id - lastData.id
    if (lastData.type === 'rollback' && timeSinceRollback < delayTime) {
      return
    }
  }

  dataHistory = await condenseHistory(dataHistory, editor)

  // Add the new data to the end of the array.
  dataHistory.push(newData)

  const compressedJSON = await compress({
    saveTime: new Date(),
    logs: dataHistory,
  })

  try {
    localStorage.setItem(storageKey, compressedJSON)
  }
  catch (e) {
    if (e instanceof DOMException && e.name === 'QuotaExceededError') {
      console.log('Browser localStorage is full, clear your storage')
    }
  }

  return true
}

/**
 * Compress JSON data to base64 encoded string.
 *
 * @param {array} data
 *
 * @returns {Promise<string>}
 */
async function compress (data) {
  // Convert JSON to Stream
  const stream = new Blob([JSON.stringify(data)], {
    type: 'application/json',
  }).stream()

  // gzip stream
  const compressedReadableStream = stream.pipeThrough(
    new CompressionStream('gzip'),
  )

  // create Response
  const compressedResponse = await new Response(compressedReadableStream)

  // Get response Blob
  const blob = await compressedResponse.blob()
  // Get the ArrayBuffer
  const buffer = await blob.arrayBuffer()
  return bufferToBase64(buffer)
}

/**
 * Decompress base64 encoded string.
 *
 * @param {string} compressedBase64
 * @returns {Promise<array>}
 */
async function decompress (compressedBase64) {
  // base64 encoding to Blob
  const stream = new Blob([base64ToBytes(compressedBase64)], {
    type: 'application/json',
  }).stream()

  const compressedReadableStream = stream.pipeThrough(
    new DecompressionStream('gzip'),
  )

  const response = await new Response(compressedReadableStream)
  const blob = await response.blob()

  return JSON.parse(await blob.text())
}

/**
 * Convert ArrayBuffer to base64 encoded string
 *
 * @param {ArrayBuffer} buffer
 * @returns {string}
 */
function bufferToBase64 (buffer) {
  const chunkSize = 65536
  const uint8Array = new Uint8Array(buffer)
  let result = ''

  for (let i = 0; i < uint8Array.length; i += chunkSize) {
    const chunk = uint8Array.slice(i, i + chunkSize)
    result += String.fromCharCode.apply(null, chunk)
  }

  return btoa(result)
}

/**
 * base64 to Uint8Array
 *
 * @param {string} base64Data
 * @returns {Uint8Array}
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
 */
function base64ToBytes (base64Data) {
  const binString = atob(base64Data)
  return Uint8Array.from(binString, (m) => m.codePointAt(0))
}

/**
 * Remove history that is older than the limit or too large.
 *
 * @param {array} dataHistory
 * @param {module:core/editor/editor~Editor} editor
 *
 * @returns {Promise<array>}
 */
async function condenseHistory (dataHistory, editor) {
  // Remove the oldest data if the length exceeds the limit.
  const limit = editor.config.get('historyLog.limit')
  if (dataHistory.length >= limit) {
    dataHistory.shift()
  }

  // Remove half of dataHistory if the data size is larger than allowed.
  const maxSize = editor.config.get('historyLog.maxSize')
  const compressedHistory = localStorage.getItem(getStorageKey(editor))
  if (compressedHistory && compressedHistory.length > maxSize) {
    const halfIndex = Math.floor(dataHistory.length / 2)
    dataHistory.splice(0, halfIndex)
  }
  return dataHistory
}

/**
 * Get the draft from the History list by id.
 *
 * @param {number} id
 * @param {array} dataHistory
 *
 * @returns {Object|null}
 */
export function getRevisionById (id, dataHistory) {
  return dataHistory.find(data => data.id === id) ?? null
}

/**
 * Remove the expired logs from the localStorage.
 *
 * @param {module:core/editor/editor~Editor} editor
 */
export async function purgeExpiredLogs (editor) {
  const currentTime = Date.now()
  const expireMinutes = editor.config.get('historyLog.expireMinutes') * 60 * 1000 // Convert minutes to milliseconds
  const prefix = editor.config.get('historyLog.saveKeyPrefix')
  const delimiter = editor.config.get('historyLog.saveKeyDelimiter')

  const keys = Object.keys(localStorage)
  for (const key of keys) {
    if (key.startsWith(prefix + delimiter)) {
      const compressedJSON = localStorage.getItem(key)
      const data = await decompress(compressedJSON)

      // Remove data that is older than expireMinutes config value.
      if (data.saveTime && currentTime - new Date(data.saveTime).getTime() > expireMinutes) {
        localStorage.removeItem(key)
      }
    }
  }
}

/**
 * Remove parameters from a url.
 *
 * @param {string} url
 *
 * @returns {string}
 */
function removeUrlParameters (url) {
  const urlObj = new URL(url)
  return urlObj.pathname
}
