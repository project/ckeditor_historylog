// Description: Utility functions for the history log plugin.

/**
 * Get the current date and time
 *
 * @param {number} date
 *   Unix timestamp.
 * @returns {string}
 *   The time since the date.
 */
export function timeSince (date) {
  const seconds = Math.floor((new Date() - date) / 1000)
  let interval = seconds / 31536000

  if (interval > 1) {
    return Math.floor(interval) + ' years'
  }
  interval = seconds / 2592000
  if (interval > 1) {
    return Math.floor(interval) + ' months'
  }
  interval = seconds / 86400
  if (interval > 1) {
    return Math.floor(interval) + ' days'
  }
  interval = seconds / 3600
  if (interval > 1) {
    return Math.floor(interval) + ' hours'
  }
  interval = seconds / 60
  if (interval > 1) {
    return Math.floor(interval) + ' minutes'
  }
  return Math.floor(seconds) + ' seconds'
}

/**
 * Generate a simple hash for some data.
 *
 * @param {string} data
 *   The data to hash.
 *
 * @returns {number}
 *
 * @see https://stackoverflow.com/a/6122571
 */
export function generateHash (data) {
  let hash = 0
  for (let i = 0, len = data.length; i < len; i++) {
    // Get the Unicode value of the character.
    let chr = data.charCodeAt(i)
    // Perform some bitwise operations to generate the hash.
    hash = (hash << 5) - hash + chr
    // Convert to 32bit integer.
    hash |= 0
  }
  return hash
}
