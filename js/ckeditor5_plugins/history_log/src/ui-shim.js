/**
 * @file Contains shims to avoid breaking older versions of CKEditor 5.
 */

import { ViewModel, Model } from 'ckeditor5/src/ui';

// Older versions of CKEditor 5 expect the export to be named `Model` rather
// than `ViewModel`. See https://ckeditor.com/docs/ckeditor5/latest/updating/guides/update-to-41.html#exports-renamed
// This shim allows this plugin to retain compatibility with Drupal 10.2.
export const UiViewModel = typeof ViewModel !== 'undefined' ? ViewModel : Model;
