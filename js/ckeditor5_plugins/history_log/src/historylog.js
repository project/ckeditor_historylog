import { Plugin } from 'ckeditor5/src/core'
import HistoryLogUI from './ui'
import {
  createStorageKey,
  getStorageKey,
  purgeExpiredLogs,
  saveData,
} from './storage'
import HistoryLogCommand from './command'

export default class HistoryLog extends Plugin {

  /**
   * @inheritDoc
   */
  static get requires () {
    return [HistoryLogUI]
  }

  /**
   * @inheritDoc
   */
  static get pluginName () {
    return 'HistoryLog'
  }

  /**
   * @inheritDoc
   */
  constructor (editor) {
    super(editor)

    editor.config.define('historyLog', {
      saveKey: 'cklog_historylog', // This will be dynamically changed.
      saveKeyPrefix: 'cklog',
      saveKeyIgnoreParams: true,
      saveKeyDelimiter: '_',
      saveKeyAttribute: 'id',
      // The time in milliseconds to wait before saving the data.
      waitingTime: 2000,
      // Should the editor save the content before restoring it?
      saveBeforeRestore: true,
      // Remove logs older than X minutes. The default 1440 is one day.
      expireMinutes: 1440,
      // Limit the number of revisions to keep.
      limit: 50,
      // The maximum size of the data to save. Default should be 250KB.
      maxSize: 250 * 1024,
      // Ignore showing revisions that match the current data.
      ignoreSameData: true,
    })

    createStorageKey(editor)

    // Alter the editor configuration for the autosave plugin.
    editor.config.set('autosave', {
      waitingTime: editor.config.get('historyLog.waitingTime'),
      save (editor) {
        // The saveData() function must return a promise
        // which should be resolved when the data is successfully saved.
        const storageKey = getStorageKey(editor)
        return saveData(storageKey, editor)
      },
    })
  }

  /**
   * @inheritDoc
   */
  init () {
    const editor = this.editor

    editor.commands.add('historyLogRevert', new HistoryLogCommand(editor))

    // When the editor is ready, purge expired logs and save the initial data.
    editor.once('ready', () => {
      const storageKey = getStorageKey(editor)
      purgeExpiredLogs(editor).then(() => {
        preventPageExitConfirmation(editor)
        saveData(storageKey, editor, 'load')
      })
    })

    // Show an alert box when notifications are used.
    const notifications = editor.plugins.get('Notification')
    notifications.on('show:info', (evt, data) => {
      alert(data.message)
      evt.stop()
    })
  }

}

/**
 * Prevent the page exit confirmation from being shown.
 *
 * @param {module:core/editor/editor~Editor} editor
 */
function preventPageExitConfirmation (editor) {
  // Autosave listens to the window#beforeunload event and will prevent a normal
  // page save if a save is happing. We do not care about this and do not want
  // a user to be prevented from saving their work if they want to.
  const autosave = editor.plugins.get('Autosave')
  autosave._domEmitter.stopListening(window, 'beforeunload')
}
