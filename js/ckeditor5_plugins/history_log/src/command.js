import { Command } from 'ckeditor5/src/core'
import { getRevisionById, getStorageKey, loadData, saveData } from './storage'

export default class HistoryLogCommand extends Command {

  /**
   * @inheritDoc
   *
   * @param {Number} id - The id of the revision to revert to.
   */
  async execute (id) {
    const editor = this.editor
    const storageKey = getStorageKey(editor)
    const notification = editor.plugins.get('Notification')

    const history = await loadData(storageKey)
    if (!history) {
      notification.showInfo('No revision history found.')
      return
    }

    const revisionData = getRevisionById(id, history)
    if (!revisionData) {
      notification.showInfo('No data found for this revision.')
      return
    }

    // Check if the revision data is the same as the current data.
    if (revisionData.data === editor.getData()) {
      notification.showInfo('The revision data is the same as the current data.')
      return
    }

    // Save the current state so that we can come back to it.
    const preSave = editor.config.get('historyLog.saveBeforeRestore')
    if (preSave) {
      await saveData(storageKey, editor, 'rollback')
    }

    // Second, load the data from the selected revision
    editor.setData(revisionData.data)
    editor.editing.view.focus()
  }

  refresh () {
    this.isEnabled = true
  }
}
