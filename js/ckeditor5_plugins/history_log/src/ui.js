/**
 * @file registers the history_log toolbar button and binds functionality to it.
 */
import { Plugin } from 'ckeditor5/src/core'
import {
  addListToDropdown,
  createDropdown,
  Notification,
} from 'ckeditor5/src/ui'
import { UiViewModel as ViewModel } from './ui-shim'
import { Collection } from 'ckeditor5/src/utils'
import { getStorageKey, loadData } from './storage'
import { generateHash, timeSince } from './utility'

import '../theme/ui.css'
import icon from '../theme/icons/history_log.svg'

export default class HistoryLogUI extends Plugin {

  /**
   * @inheritDoc
   */
  static get requires () {
    return [Notification]
  }

  init () {
    const editor = this.editor
    const t = editor.t

    // Add the "autsaveDraft" to feature components.
    editor.ui.componentFactory.add('historyLog', locale => {
      const command = editor.commands.get('historyLogRevert')
      const dropdownView = createDropdown(locale)
      const accessibleLabel = t('History Log Revert')

      // Create a dropdown with a list inside the panel.
      addListToDropdown(dropdownView, new Collection(), {
        role: 'menu',
        ariaLabel: accessibleLabel,
      })

      // Create dropdown model.
      dropdownView.buttonView.set({
        label: accessibleLabel,
        icon,
        tooltip: true,
      })

      dropdownView.extendTemplate({
        attributes: {
          class: [
            'ck-history-log-dropdown',
          ],
        },
      })

      dropdownView.bind('isEnabled').to(command, 'isEnabled')

      // Populate the dropdown with the history when the button is clicked.
      this.listenTo(dropdownView.buttonView, 'execute', async () => {
        const dropdownItems = await _createDropdownOption(editor)
        dropdownView.panelView.element.textContent = ''
        addListToDropdown(dropdownView, dropdownItems, {
          role: 'menu',
          ariaLabel: accessibleLabel,
        })
      })

      // Insert the saved data into the editor after clicking the button.
      this.listenTo(dropdownView, 'execute', (evt) => {
        editor.execute('historyLogRevert', evt.source.value)
      })

      return dropdownView
    })
  }

}

/**
 * Create dropdown options of each revision for the dropdown panel.
 *
 * @param {module:core/editor/editor~Editor} editor
 *
 * @returns {Promise<module:utils/collection~Collection>}
 */
async function _createDropdownOption (editor) {
  const itemDefinitions = new Collection()
  const storageKey = getStorageKey(editor)
  const history = await loadData(storageKey)
  const currentDataHash = generateHash(editor.getData())

  if (!history) {
    return itemDefinitions
  }

  history.reverse()

  history.forEach(data => {
    const sameData = currentDataHash === data.dataHash

    // Ignore data that is the same as the current data.
    if (sameData && editor.config.get('historyLog.ignoreSameData')) {
      return
    }

    let label = timeSince(data.id) + ' ago'
    if (data.type !== 'autosave') {
      label += ' - ' + data.type
    }

    const def = {
      type: 'button',
      model: new ViewModel({
        value: data.id,
        label,
        withText: true,
      }),
    }
    if (sameData) {
      def.model.set('class', 'ck-history-log-dropdown__same-data')
    }

    itemDefinitions.add(def)

    if (data.type === 'load') {
      itemDefinitions.add({
        type: 'separator',
      })
    }

  })

  return itemDefinitions
}
