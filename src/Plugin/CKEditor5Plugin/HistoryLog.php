<?php

namespace Drupal\ckeditor_historylog\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\EditorInterface;

/**
 * Plugin class to add an autosave history log.
 */
class HistoryLog extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'saveKeyAttribute' => 'name',
      'waitingTime' => 5000,
      'expireMinutes' => 1440,
      'limit' => 50,
      'maxSize' => 256000,
      'saveBeforeRestore' => TRUE,
      'ignoreSameData' => TRUE,
    ];
  }

  /**
   * Loads plugin configuration from Drupal.
   *
   * @param array $static_plugin_config
   *   The static plugin configuration.
   * @param \Drupal\editor\EditorInterface $editor
   *   The editor instance.
   *
   * @return array|array[]
   *   The dynamic plugin configuration.
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $config = $static_plugin_config;
    $config += [
      'historyLog' => [
        'saveKeyAttribute' => $this->configuration['saveKeyAttribute'],
        'waitingTime' => $this->configuration['waitingTime'],
        'expireMinutes' => $this->configuration['expireMinutes'],
        'limit' => $this->configuration['limit'],
        'maxSize' => $this->configuration['maxSize'],
        'saveBeforeRestore' => $this->configuration['saveBeforeRestore'],
        'ignoreSameData' => $this->configuration['ignoreSameData'],
      ],
    ];
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['saveKeyAttribute'] = [
      '#type' => 'textfield',
      '#default_value' => $this->configuration['saveKeyAttribute'],
      '#title' => $this->t('Field attribute selector'),
      '#description' => $this->t('The textarea field attribute used to get the unique ckeditor instance. Ex: id, name, class, etc.'),
    ];
    $form['waitingTime'] = [
      '#type' => 'number',
      '#default_value' => $this->configuration['waitingTime'],
      '#title' => $this->t('The time in milliseconds to wait before saving the data'),
    ];
    $form['expireMinutes'] = [
      '#type' => 'number',
      '#default_value' => $this->configuration['expireMinutes'],
      '#title' => $this->t('Amount of minutes to keep logs before they are automatically removed'),
      '#description' => $this->t('The default 1440 minutes is one day.'),
    ];
    $form['limit'] = [
      '#type' => 'number',
      '#default_value' => $this->configuration['limit'],
      '#title' => $this->t('Maximum number of revisions to keep'),
    ];
    $form['maxSize'] = [
      '#type' => 'number',
      '#default_value' => $this->configuration['maxSize'],
      '#title' => $this->t('Maximum storage size in bytes'),
      '#description' => $this->t('If the storage size exceeds this value, half of the revisions will be removed.'),
    ];
    $form['saveBeforeRestore'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save before restoring a previous revision'),
      '#default_value' => $this->configuration['saveBeforeRestore'],
      '#description' => $this->t('If enabled, the current content will be saved before restoring a previous revision and tagged with "rollback".'),
    ];
    $form['ignoreSameData'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore revisions that match the current data'),
      '#default_value' => $this->configuration['ignoreSameData'],
      '#description' => $this->t('If enabled, revisions that match the current data will not be shown in the history log to reduce clutter.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $number_fields = ['waitingTime', 'expireMinutes', 'limit', 'maxSize'];
    foreach ($number_fields as $field) {
      $form_state->setValue($field, (int) $form_state->getValue($field));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['saveKeyAttribute'] = $form_state->getValue('saveKeyAttribute');
    $this->configuration['waitingTime'] = $form_state->getValue('waitingTime');
    $this->configuration['expireMinutes'] = $form_state->getValue('expireMinutes');
    $this->configuration['limit'] = $form_state->getValue('limit');
    $this->configuration['maxSize'] = $form_state->getValue('maxSize');
    $this->configuration['saveBeforeRestore'] = $form_state->getValue('saveBeforeRestore');
    $this->configuration['ignoreSameData'] = $form_state->getValue('ignoreSameData');
  }

}
