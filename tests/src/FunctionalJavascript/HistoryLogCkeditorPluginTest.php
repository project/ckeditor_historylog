<?php

namespace Drupal\Tests\ckeditor_historylog\FunctionalJavascript;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\ckeditor5\Traits\CKEditor5TestTrait;
use Drupal\Tests\ckeditor_historylog\Traits\HistoryLogTestHelperTrait;

/**
 * Contains history_log CKEditor plugin functionality tests.
 *
 * @group ckeditor_historylog
 */
class HistoryLogCkeditorPluginTest extends WebDriverTestBase {

  use StringTranslationTrait;
  use CKEditor5TestTrait;
  use HistoryLogTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'ckeditor5',
    'filter',
    'ckeditor5_test',
    'ckeditor_historylog',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpFormatAndEditor();
    $this->setUpNodeTypeAndField();
    $this->setUpDefaultUserWithAccess();
  }

  /**
   * Tests CKEditor5 plugin loads the History Log button.
   */
  public function testUiLoads() {
    $assert_session = $this->assertSession();

    $this->drupalGet('node/add/page');
    $this->waitForEditor();

    $this->assertEditorButtonEnabled('History Log Revert');

    // Open the history panel and check it contains a revert revision button.
    $this->pressHistoryLogButton();
    $assert_session->elementTextContains('css', '.ck-history-log-dropdown .ck-dropdown__panel-visible li:first-child button', $this->t('seconds ago - load'));
  }

  /**
   * Tests that data is autosaved after a change.
   */
  public function testAutosave() {
    $assert_session = $this->assertSession();

    $this->drupalGet('node/add/page');
    $this->waitForEditor();

    // Make a text change to the editor.
    $this->setEditorData('This is a test change.');
    $this->waitForAutosave();

    $this->pressHistoryLogButton();

    // Ensure the first revision in the history panel contains the text change.
    $selector = '.ck-history-log-dropdown .ck-dropdown__panel-visible li:first-child button';
    $assert_session->elementTextContains('css', $selector, 'seconds ago');
    $assert_session->elementTextNotContains('css', $selector, ' - load');
  }

  /**
   * Tests that history from one editor does not spill into the second editor.
   */
  public function testEditorHistoryIsolation() {
    // Button selector for all history log panels.
    $button_selector = '.ck-history-log-dropdown .ck-dropdown__panel-visible li:nth-child(2) button';

    $this->drupalGet('node/add/page');
    $this->waitForEditor();

    // Make a text change to the first editor.
    $editor1_change1 = 'First editor change #1.';
    $this->setEditorData($editor1_change1);
    $this->waitForAutosave();

    $data = $this->getEditorData();
    $this->assertStringContainsString($editor1_change1, $data);

    // Make a second text change to the first editor.
    $this->setEditorData('First editor change #2.');
    $this->waitForAutosave();

    // Make a text change to the second editor.
    $editor2_change1 = 'Second editor change #1.';
    $this->setEditorData($editor2_change1, '#edit-body2-0-value');
    $this->waitForAutosave();

    $data = $this->getEditorData('#edit-body2-0-value');
    $this->assertStringContainsString($editor2_change1, $data);

    // Make a second text change to the second editor.
    $this->setEditorData('Second editor change #2.', '#edit-body2-0-value');
    $this->waitForAutosave();

    // Revert the first editor to its first change.
    $this->pressHistoryLogButton();
    $this->pressPanelRevisionButton($button_selector);
    $data = $this->getEditorData();
    $this->assertStringContainsString($editor1_change1, $data);

    // Revert the second editor to its first change.
    $this->pressHistoryLogButton('edit-body2-0-value');
    $this->pressPanelRevisionButton($button_selector);
    $data = $this->getEditorData('#edit-body2-0-value');
    $this->assertStringContainsString($editor2_change1, $data);
  }

}
