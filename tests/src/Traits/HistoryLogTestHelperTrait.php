<?php

namespace Drupal\Tests\ckeditor_historylog\Traits;

use Drupal\editor\Entity\Editor;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\node\Entity\NodeType;

/**
 * Helper trait used by multiple tests.
 */
trait HistoryLogTestHelperTrait {

  /**
   * The FilterFormat config entity used for testing.
   *
   * @var \Drupal\filter\FilterFormatInterface
   */
  protected $filterFormat;

  /**
   * The account.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * Set up the text format and associate an editor.
   */
  protected function setUpFormatAndEditor(): void {
    // Create a text format and associate CKEditor.
    $this->filterFormat = FilterFormat::create([
      'format' => 'filtered_html',
      'name' => 'Filtered HTML',
    ]);
    $this->filterFormat->save();

    Editor::create([
      'format' => 'filtered_html',
      'editor' => 'ckeditor5',
      'settings' => [
        'toolbar' => [
          'items' => [
            'bold',
            'italic',
            'historyLog',
          ],
        ],
        'plugins' => [
          'ckeditor_historylog_button' => [
            'saveKeyAttribute' => 'name',
            'waitingTime' => 10,
            'expireMinutes' => 3,
            'limit' => 3,
            'maxSize' => 256000,
            'saveBeforeRestore' => TRUE,
            'ignoreSameData' => FALSE,
          ],
        ],
      ],
    ])->save();
  }

  /**
   * Set up the node type and field.
   */
  protected function setUpNodeTypeAndField(): void {
    // Create a node type for testing.
    NodeType::create([
      'type' => 'page',
      'name' => 'page',
    ])->save();

    // Create a body field instance for the 'page' node type.
    $field_storage = FieldStorageConfig::loadByName('node', 'body');
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'page',
      'label' => 'Body',
      'settings' => ['display_summary' => TRUE],
      'required' => TRUE,
    ])->save();

    // Create a second body field instance.
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'body2',
      'type' => 'text_with_summary',
      'entity_type' => 'node',
      'cardinality' => 1,
      'settings' => [],
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_name' => 'body2',
      'field_storage' => $field_storage,
      'bundle' => 'page',
      'label' => 'Body2',
      'settings' => ['display_summary' => TRUE],
      'required' => TRUE,
    ])->save();

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    // Create a form display for the default form mode.
    $display_repository->getFormDisplay('node', 'page')
      ->setComponent('body', [
        'type' => 'text_textarea_with_summary',
      ])
      ->setComponent('body2', [
        'type' => 'text_textarea_with_summary',
      ])
      ->save();

    // Create a display for the full view mode.
    $display_repository->getViewDisplay('node', 'page', 'full')
      ->setComponent('body', [
        'type' => 'text_default',
        'settings' => [],
      ])
      ->setComponent('body2', [
        'type' => 'text_default',
        'settings' => [],
      ])
      ->save();
  }

  /**
   * Set up default user with access to the text format.
   */
  protected function setUpDefaultUserWithAccess(): void {
    $this->account = $this->drupalCreateUser([
      'administer nodes',
      'create page content',
      'use text format filtered_html',
    ]);
    $this->drupalLogin($this->account);
  }

  /**
   * Press the History Log button in the CKEditor toolbar.
   *
   * @param string $parentSelectorId
   *   The selector to use for the textarea that is being altered.
   */
  protected function pressHistoryLogButton(string $parentSelectorId = 'edit-body-0-value'): void {
    // Remove a possible hash from the beginning of the selector.
    $parentSelectorId = ltrim($parentSelectorId, '#');

    $name = 'History Log Revert';
    $button = $this->assertSession()->waitForElementVisible('xpath', "//*[@id='$parentSelectorId']/following-sibling::div[1]//button[span[text()='$name']]");
    $this->assertNotEmpty($button);
    $button->click();

    $this->waitForHistoryPanel();
  }

  /**
   * Wait for the History Log panel to open.
   */
  protected function waitForHistoryPanel(): void {
    $panel_div = $this->assertSession()
      ->waitForElementVisible('css', '.ck-history-log-dropdown .ck-dropdown__panel-visible');
    $this->assertNotNull($panel_div, 'History Log panel was opened');
  }

  /**
   * Press a revision button in the CKEditor History Log panel.
   *
   * @param string $button_selector
   *   The css selector to use for the button that is being pressed.
   */
  public function pressPanelRevisionButton(string $button_selector): void {
    $button = $this->assertSession()
      ->waitForElementVisible('css', $button_selector);
    $this->assertNotEmpty($button);
    $button->click();
    $this->waitForAutosave();
  }

  /**
   * Set the editor data.
   *
   * @param string $text_change
   *   The text to set.
   * @param string $selector
   *   The selector to use for the textarea that is being altered.
   */
  protected function setEditorData(string $text_change, string $selector = '#edit-body-0-value'): void {
    $this->getSession()
      ->executeScript("document.querySelector('$selector + .ck-editor .ck-editor__editable').ckeditorInstance.setData('$text_change');");
  }

  /**
   * Gets CKEditor 5 instance data as a HTML string.
   *
   * @param string $selector
   *   The selector to use for the textarea that is being altered.
   *
   * @return string
   *   The result of retrieving CKEditor 5's data.
   *
   * @see https://ckeditor.com/docs/ckeditor5/latest/api/module_editor-classic_classiceditor-ClassicEditor.html#function-getData
   */
  protected function getEditorData(string $selector = '#edit-body-0-value'): string {
    // We cannot trust on CKEditor updating the textarea every time model
    // changes. Therefore, the most reliable way to get downcasted data is to
    // use the CKEditor API.
    $javascript = <<<JS
(function(){
  return document.querySelector('$selector + .ck-editor .ck-editor__editable').ckeditorInstance.getData();
})();
JS;
    return $this->getSession()->evaluateScript($javascript);
  }

  /**
   * Wait for the editor to autosave the current data.
   */
  protected function waitForAutosave(): void {
    usleep(50 * 1000);
  }

}
